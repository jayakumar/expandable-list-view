package com.jenny.expandablerecycleradapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public abstract class ChildViewHolder extends RecyclerView.ViewHolder {

    public ChildViewHolder(@NonNull View itemView) {
        super(itemView);
    }
}