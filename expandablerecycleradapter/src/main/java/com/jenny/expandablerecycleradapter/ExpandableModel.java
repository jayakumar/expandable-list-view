package com.jenny.expandablerecycleradapter;

import java.util.List;

public class ExpandableModel<D extends ExpandModel> implements ExpandModel {

    private List<D> mChildModel;

    private boolean mIsExpanded = false;

    public List<D> getChildModel() {
        return mChildModel;
    }

    public void setChildModel(List<D> mChildModel) {
        this.mChildModel = mChildModel;
    }

    public boolean isExpanded() {
        return mIsExpanded;
    }

    public void setExpanded(boolean expanded) {
        mIsExpanded = expanded;
    }

        @Override
    public boolean isParent() {
        return true;
    }
}
