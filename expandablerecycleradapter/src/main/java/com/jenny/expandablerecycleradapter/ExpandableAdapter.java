package com.jenny.expandablerecycleradapter;

import android.support.annotation.NonNull;
import android.support.v4.util.ArrayMap;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.ArrayList;

public abstract class ExpandableAdapter<CM extends ExpandModel, CH extends ChildViewHolder,
        PV extends ExpandedViewHolder> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<? extends ExpandableModel<CM>> mExpandableModel;

    private ArrayMap<Integer, Integer> mViewTypes = new ArrayMap<>();
    private ArrayMap<Integer, Integer> mGroupTypes = new ArrayMap<>();

    @interface ViewTypes {
        int TYPE_CHILD = 1;
        int TYPE_PARENT = 0;
    }

    public ExpandableAdapter(ArrayList<? extends ExpandableModel<CM>> expandableModel) {
        this.mExpandableModel = expandableModel;
    }

    @NonNull
    protected abstract PV onCreateExpandedViewHolder(@NonNull ViewGroup viewGroup, int viewType);

    protected abstract CH onCreateChildViewHolder(@NonNull ViewGroup viewGroup, int viewType);

    protected abstract void onBindExpandedViewHolder(PV viewHolder, ExpandableModel<CM> expandedModel);

    protected abstract void onBindChildViewHolder(CH viewHolder, CM childModel, int position);

    protected int getChildViewTypes(ExpandableModel<CM> model, int position) {
        return ViewTypes.TYPE_CHILD;
    }

    protected int getParentViewTypes(ExpandableModel<CM> model) {
        return ViewTypes.TYPE_PARENT;
    }

    public void expandItem(int position) {
        mExpandableModel.get(position).setExpanded(true);
        int newSize = mExpandableModel.get(position).getChildModel().size();
        notifyItemRangeInserted(position, newSize);
    }

    public void clollapseItem(int position) {
        mExpandableModel.get(position).setExpanded(false);
        int deletedSize = mExpandableModel.get(position).getChildModel().size();
        notifyItemRangeRemoved(position, deletedSize);
    }

    @NonNull
    @Override
    public final RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        if (mGroupTypes.containsValue(viewGroup)) {
            return onCreateExpandedViewHolder(viewGroup, viewType);
        } else {
            return onCreateChildViewHolder(viewGroup, viewType);
        }
    }

    @Override
    public final void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder instanceof ExpandedViewHolder) {
            onBindExpandedViewHolder((PV) viewHolder, mExpandableModel.get(position));
        } else {
            for (int i = 0; i < mExpandableModel.size(); i++) {
                ExpandableModel<CM> em = mExpandableModel.get(i);
                if (em.isExpanded() && position > i && position <= i + em.getChildModel().size()) {
                    onBindChildViewHolder((CH) viewHolder, em.getChildModel().get(i), i);
                }
            }
        }
    }

    @Override
    public final int getItemViewType(int position) {
        int viewType = 0;
        if (mExpandableModel != null) {
            if (position <= mExpandableModel.size()) {
                viewType = getParentViewTypes(mExpandableModel.get(position));
                mGroupTypes.put(position, viewType);
            } else {
                for (int i = 0; i < mExpandableModel.size(); i++) {
                    ExpandableModel<CM> em = mExpandableModel.get(i);
                    if (em.isExpanded() && (position > i && position <= i + em.getChildModel().size())) {
                        viewType = getChildViewTypes(em, position);
                    }
                }
                mViewTypes.put(position, viewType);
            }
        }
        return viewType;
    }

    @Override
    public final int getItemCount() {
        int size = 0;
        if (mExpandableModel != null) {
            size = mExpandableModel.size();
            for (ExpandableModel em : mExpandableModel) {
                if (em.isExpanded()) {
                    size += em.getChildModel().size();
                }
            }
        }
        return size;
    }
}
