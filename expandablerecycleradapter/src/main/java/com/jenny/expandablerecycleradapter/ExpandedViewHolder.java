package com.jenny.expandablerecycleradapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public abstract class ExpandedViewHolder extends RecyclerView.ViewHolder {

    private ExpandableModel mExpandableModel;

    public ExpandedViewHolder(@NonNull View itemView) {
        super(itemView);
    }

    final void setExpandableModel(ExpandableModel mExpandableModel) {
        this.mExpandableModel = mExpandableModel;
    }

    final ExpandableModel getExpandableModel() {
        return mExpandableModel;
    }
}
