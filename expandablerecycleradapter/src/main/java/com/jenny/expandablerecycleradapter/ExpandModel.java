package com.jenny.expandablerecycleradapter;

public interface ExpandModel {

    default boolean isParent() {
        return false;
    }

    default boolean isChild() {
        return false;
    }

    default boolean isExpanded() {
        return false;
    }
}
