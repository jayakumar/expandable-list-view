package com.jinu.expandablerecyclerview;

import com.jenny.expandablerecycleradapter.ExpandModel;

public class ChildModel implements ExpandModel {

    public String data;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public boolean isChild() {
        return true;
    }
}
