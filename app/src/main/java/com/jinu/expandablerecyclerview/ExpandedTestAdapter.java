package com.jinu.expandablerecyclerview;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jenny.expandablerecycleradapter.ChildViewHolder;
import com.jenny.expandablerecycleradapter.ExpandableAdapter;
import com.jenny.expandablerecycleradapter.ExpandableModel;
import com.jenny.expandablerecycleradapter.ExpandedViewHolder;

import java.util.ArrayList;


public class ExpandedTestAdapter extends ExpandableAdapter<ChildModel,
        ExpandedTestAdapter.ChildExtendedVH,
        ExpandedTestAdapter.ExpandedExtendVH> implements ClickListener {



    @Override
    public void onClick(int position) {

    }

    protected ExpandedTestAdapter(ArrayList<? extends ExpandableModel<ChildModel>> expandableModel) {
        super(expandableModel);
    }

    @NonNull
    @Override
    public ExpandedTestAdapter.ExpandedExtendVH onCreateExpandedViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(android.R.layout.simple_list_item_1, viewGroup, false);
        return new ExpandedExtendVH(view, this);
    }

    @Override
    public ExpandedTestAdapter.ChildExtendedVH onCreateChildViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(android.R.layout.simple_list_item_1, viewGroup, false);
        return new ChildExtendedVH(view);
    }

    @Override
    public void onBindExpandedViewHolder(ExpandedTestAdapter.ExpandedExtendVH viewHolder, ExpandableModel<ChildModel> expandedModel) {
        viewHolder.mText.setText(String.valueOf(expandedModel));
    }

    @Override
    public void onBindChildViewHolder(ExpandedTestAdapter.ChildExtendedVH viewHolder, ChildModel childModel, int position) {
        viewHolder.mText.setText(String.valueOf(childModel));
    }

    static class ChildExtendedVH extends ChildViewHolder {

        private TextView mText;

        ChildExtendedVH(@NonNull View itemView) {
            super(itemView);
            mText = itemView.findViewById(android.R.id.text1);
        }
    }


    static class ExpandedExtendVH extends ExpandedViewHolder implements View.OnClickListener {

        private TextView mText;
        private ClickListener mClickListener;

        ExpandedExtendVH(@NonNull View itemView, ClickListener clickListener) {
            super(itemView);
            mText = itemView.findViewById(android.R.id.text1);
            mClickListener = clickListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mClickListener.onClick(getAdapterPosition());
        }
    }
}
