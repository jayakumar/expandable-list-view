package com.jinu.expandablerecyclerview;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.transition.AutoTransition;
import android.transition.ChangeBounds;
import android.transition.Fade;
import android.transition.TransitionManager;
import android.transition.TransitionSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

public class ChangeBoundsActivity extends AppCompatActivity {

    private LinearLayout layou1;
    private LinearLayout layou2;
    private ViewGroup parent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_bounds);
        layou1 = findViewById(R.id.layou1);
        layou2 = findViewById(R.id.layou2);
        parent = findViewById(R.id.androoifd);
        Runnable runnable = () -> {
            TransitionManager.beginDelayedTransition(parent);
            ViewGroup.LayoutParams params = layou1.getLayoutParams();
            params.width = (int) (getResources().getDisplayMetrics().density * 500);
            layou1.setLayoutParams(params);
            layou2.setVisibility(View.VISIBLE);
            startAnother();
        };
        new Handler().postDelayed(runnable, 3000);
    }

    private void startAnother() {
        Runnable runnable = () -> {
            TransitionManager.beginDelayedTransition(parent);
            ViewGroup.LayoutParams params = layou1.getLayoutParams();
            params.width = (int) (getResources().getDisplayMetrics().density * 300);
            layou1.setLayoutParams(params);
        };
        new Handler().postDelayed(runnable, 3000);
    }

    public static final int DURATION = 2000;

    public static void beginAuto(ViewGroup viewGroup) {
        AutoTransition transition = new AutoTransition();
        transition.setDuration(DURATION);
        transition.setOrdering(TransitionSet.ORDERING_TOGETHER);
        TransitionManager.beginDelayedTransition(viewGroup, transition);
    }

    public static void beginFade(ViewGroup viewGroup) {
        Fade transition = new Fade();
        transition.setDuration(DURATION);
        TransitionManager.beginDelayedTransition(viewGroup, transition);
    }

    public static void beginChangeBounds(ViewGroup viewGroup) {
        ChangeBounds transition = new ChangeBounds();
        transition.setDuration(DURATION);
        TransitionManager.beginDelayedTransition(viewGroup, transition);
    }
}
