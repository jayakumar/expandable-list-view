package com.jinu.expandablerecyclerview;

public interface ClickListener {
    void onClick(int position);
}
